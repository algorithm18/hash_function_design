import java.util.ArrayList;

public class HashmapFunction {
    int M;
    ArrayList<String> arrlist = new ArrayList<String>();
    
    public HashmapFunction(int m){
        this.M = m;

        for(int i=0 ;i < m ;i ++){
            arrlist.add(i,null);
        }

    }
    public int hashFunction(int x){
          int result = x % M ;
        return result;
    }
    public String get(int x){
        int h = hashFunction(x);
        return arrlist.get(x);
    }
    public void getAll(){
        for(int i =0 ;i <M;i ++){
            System.out.println("[ KEY : " + i + ", VALUE : " + arrlist.get(i) + " ]");
        }
    }
    public void put(int k, String v){
        int hash = hashFunction(k);
        arrlist.set(hash, v);
    }

    public void remove(int k){
        int h = hashFunction(k);
        arrlist.set(h, null);
    }
    public static void main(String[] args) {
        HashmapFunction hashmap = new HashmapFunction(30);
        hashmap.put(2, "Apple");
        hashmap.put(8, "Mango");
        hashmap.put(11, "Red");
        hashmap.put(25, "Black");
        hashmap.remove(11); 
        System.out.println(hashmap.get(25)); 
        hashmap.getAll();

    }
}
